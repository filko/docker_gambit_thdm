**Get Started**

Run the script `build_thdm_docker.sh` to generate and build a docker container with the THDM branch.

**Build HEPLike**

Once finished run the container and move/run the script `make_heplike_script.sh` tp build the heplike backend.
(Docker will not find root headers if you try to make heplike during docker build process).

**Other**

Note you can also use:
`$ build_thdm_docker.sh <username> <password> <cores>`
To start the build process.