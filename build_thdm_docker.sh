#!/bin/bash

if [[ -z "$1" || -z "$2" || -z "$3" ]]
then
    read -p 'HEPforge Username: ' uservar
    read -sp 'HEPforge Password: ' passvar
    echo 
    read -p 'Number of cores for compilation: ' corenum 
else
    uservar=$1
    passvar=$2
    corenum=$3
fi

echo 
eval "sed -e 's/USER/$(echo $uservar)/g' -e 's/PASS/$(echo $passvar)/g' -e 's/CORES/$(echo $corenum)/g' Dockerfile_THDM_template " > Dockerfile

docker build -t gambit_thdm:initial_build .