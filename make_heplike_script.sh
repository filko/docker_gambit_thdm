#!/bin/bash

# install heplike (do some funky substitutions git add .o get root include path into the BOSS script)
cd /root/gambit_THDM/build && \
mv /root/gambit_THDM/Backends/scripts/BOSS/configs/heplike_1_0.py /root/gambit_THDM/Backends/scripts/BOSS/configs/heplike_1_0_backup.py && \
eval "sed 's/include_paths = \[/include_paths = \[\x27$(root-config --incdir | sed 's/\//\\\//g')\x27,/g' /root/gambit_THDM/Backends/scripts/BOSS/configs/heplike_1_0_backup.py" > /root/gambit_THDM/Backends/scripts/BOSS/configs/heplike_1_0.py && \
make heplikedata && \
make heplike 